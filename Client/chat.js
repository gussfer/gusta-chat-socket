// FILE /client/chat.js

console.log('chat.js file loaded!')

/*IMPORTANT! 
Por padrão, socket.io() se conecta ao host que
serviu a página, então não temos que passar o url do servidor*/
let socket = io.connect()

//Pergunta nome do usuário
const username = prompt('Olá! Por favor, insira seu nome: ')

// Função 'emit': informa ao servidor o evento nome do usuário
socket.emit('new-connection', { username })

// Função "on": captura eventos enviados do servidor para os usuário
socket.on('welcome-message', (data) => {
  console.log('received welcome-message >>', data)
  addMessage(data, false)
})
  
/*
Como regra geral, lembre-se de ter uma função socket.on() 
para cada evento que você enviar com socket.emit()*/

// recebe dois parametros: a mensagem e se foi enviado por voce
function addMessage(data, isSelf = false) { // função para anexar mensagens ao 'container'
  const messageElement = document.createElement('div')
  messageElement.classList.add('message')

  if (isSelf) { // metodo para diferenciar mensagens dos diferentes users
    messageElement.classList.add('self-message')
    messageElement.innerText = `${data.message}`
  } else {
    if (data.user === 'server') {
      // message is from the server, like a notification of new user connected
      // messageElement.classList.add('others-message')
      messageElement.innerText = `${data.message}`
    } else {
      // message is from other user
      messageElement.classList.add('others-message')
      messageElement.innerText = `${data.user}: ${data.message}`
    }
  }
  // get chatContainer element from our html page
  const chatContainer = document.getElementById('chatContainer')

  // adds the new div to the message container div
  chatContainer.append(messageElement)
  }

  const messageForm = document.getElementById('messageForm')

  messageForm.addEventListener('submit', (e) => {
    // avoids submit the form and refresh the page
    e.preventDefault()
  
    const messageInput = document.getElementById('messageInput')
  
    // check if there is a message in the input
    if (messageInput.value !== '') {
      let newMessage = messageInput.value
      //sends message and our id to socket server
      socket.emit('new-message', { user: socket.id, message: newMessage })
      // appends message in chat container, with isSelf flag true
      addMessage({ message: newMessage }, true)
      //resets input
      messageInput.value = ''
    } else {
      // adds error styling to input
      messageInput.classList.add('error')
    }
  })
  
  socket.on('broadcast-message', (data) => {
    console.log('📢 broadcast-message event >> ', data)
    // appends message in chat container, with isSelf flag false
    addMessage(data, false)
  })