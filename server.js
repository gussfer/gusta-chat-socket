// Modulo que trata as requisições e serve os arquivos contidos na pasta "Client"

let http = require('http') // protocolo para rodar página
let fs = require('fs') // interage com pastas locais 
let path = require('path') // interage com caminhos das pastas
const APP_PORT = process.env.APP_PORT || 3000
const app = http.createServer(requestHandler)

app.listen(APP_PORT)
console.log(`HTTP Server rodando localmente. Porta: ${APP_PORT}`) //mostra quando inicia aplicação

// lida com todas as solicitações http para o servidor
function requestHandler(request, response) {
  console.log(`🖥 Received request for ${request.url}`)
  // append /client to serve pages from that folder
  let filePath = './client' + request.url
  if (filePath == './client/') {
    // serve index page on request /
    filePath = './client/index.html'
  }
  let extname = String(path.extname(filePath)).toLowerCase()
  console.log(`🖥 Serving ${filePath}`)
  let mimeTypes = {
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpg',
    '.gif': 'image/gif',
    '.svg': 'image/svg+xml',
  }
  let contentType = mimeTypes[extname] || 'application/octet-stream'
  fs.readFile(filePath, function (error, content) {
    if (error) {
      if (error.code == 'ENOENT') {
        fs.readFile('./client/404.html', function (error, content) {
          response.writeHead(404, { 'Content-Type': contentType })
          response.end(content, 'utf-8')
        })
      } else {
        response.writeHead(500)
        response.end('Sorry, there was an error: ' + error.code + ' ..\n')
      }
    } else {
      response.writeHead(200, { 'Content-Type': contentType })
      response.end(content, 'utf-8')
    }
  })
}

// TRATAMENTO DE EVENTOS NO CHAT SOCKET.IO
const io = require('socket.io')(app, {
  path: '/socket.io',
})

io.attach(app, {
  // includes local domain to avoid CORS error locally
  // configure it accordingly for production
  cors: {
    origin: 'http://localhost',
    methods: ['GET', 'POST'],
    credentials: true,
    transports: ['websocket', 'polling'],
  },
  allowEIO3: true,
})

io.on('connection', (socket) => {
  console.log('👾 New socket connected! >>', socket.id)
})

// To save the list of users as id:username
let users = {}

io.on('connection', (socket) => {
  console.log('👾 New socket connected! >>', socket.id)

  // handles new connection
  socket.on('new-connection', (data) => {
    // captures event when new clients join
    console.log(`new-connection event received`, data)
    // adds user to list
    users[socket.id] = data.username
    console.log('users :>> ', users)
    // emit welcome message event
    socket.emit('welcome-message', {
      user: 'server',
      message: `Bem vinde ao Gusta Chat, ${data.username}. 
      No momento, ${
        Object.keys(users).length // qtd de users conectados
      } usuários estão conectados`,
    })
    // Inserir notificação de novo usuário a cada novo user 
    socket.broadcast.emit('broadcast-message', {
      user: 'server',
      message:  `Novo usuário conectado: ${data.username}. 
      No momento, ${
        Object.keys(users).length
      } usuários estão conectados`,
    })
  })
  // handles message posted by client
  socket.on('new-message', (data) => {
    console.log(`👾 new-message from ${data.user}`)
    // broadcast message to all sockets except the one that triggered the event
    socket.broadcast.emit('broadcast-message', {
      user: users[data.user],
      message: data.message,
    })
  })
})
